main.o: main.asm lib.inc words.inc colon.inc
	nasm -f elf64 -o main.o main.asm

%.o: %.asm
	nasm -f elf64 -o $@ $<

main: main.o dict.o lib.o
	ld -o main $^

.PHONY: clean

clean:
	rm *.o
