extern lib.inc

global find_word

find_word:

	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		jz .success
		mov rsi, [rsi]
		cmp rsi, 0
		jz .not_founded
		jmp .loop

	.success:
		mov rax, rsi
		ret

	.not_founded:
		xor rax, rax
		ret
