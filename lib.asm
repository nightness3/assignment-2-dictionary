global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
    	.count:
    		cmp byte[rdi+rax], 0
		je .done
		inc rax
		jmp .count
    	.done:
    		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi ; store rdi 
	call string_length
	pop rdi ; restore rdi
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	pop rdi
	xor rax, rax
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 10
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov rcx, 10
	mov rdi, rsp
	sub rsp, 21
	dec rdi
	mov byte[rdi], 0
	.division:
		xor rdx, rdx
		div rcx
		add dl, '0'
		dec rdi
		mov byte[rdi], dl
		cmp rax, 0
		jne .division
	call print_string
	add rsp, 21
	ret
		 
		
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	js .negative
	
	jmp print_uint

	.negative:
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
		jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov rcx, 0
	.check_loop:
		mov al, byte[rdi+rcx]
		mov dl, byte[rsi+rcx]
		cmp al, 0
		je .final_check
		cmp dl, 0
		je .final_check
		cmp al, dl
		jne .not_equal
		inc rcx
		jmp .check_loop

	.final_check:
		cmp al, dl
		jne .not_equal
		mov rax, 1
		ret
	
	.not_equal:
		mov rax, 0
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
	push rax
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rdx, rdx
	xor rcx, rcx
	.main_loop:
		cmp rcx, rsi
		jge .buffer_error
		push rdi
		push rsi
		push rcx
		call read_char
		pop rcx
		pop rsi
		pop rdi
		cmp rax, ' '
		je .skip_or_finish
		cmp rax, '	'
		je .skip_or_finish
		cmp rax, '\n'
		je .skip_or_finish
		mov byte[rdi+rcx], al
		cmp al, 0
		je .finish
		inc rcx
		jmp .main_loop

	.skip_or_finish:
		test rcx, rcx
		jz .main_loop

	.finish:
		mov rdx, rcx
		mov rax, rdi
		ret	

	.buffer_error:
		xor rax, rax
		ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rsi, rsi
	xor rdx, rdx
	xor rcx, rcx
	mov r8, 10
	.read_loop:
		mov sil, byte[rdi+rcx]
		sub sil, '0'
		cmp sil, 9
		ja .not_digit
		mul r8
		add rax, rsi
		inc rcx
		jmp .read_loop

	.not_digit:
		mov rdx, rcx
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rsi, rsi
	mov sil, byte[rdi]
	cmp sil, '-'
	jne parse_uint
	
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret		

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor rcx, rcx
	xor r8, r8
	.copy_loop:
		cmp rcx, rdx
		jge .buffer_error
		mov r8b, byte[rdi+rcx]
		mov byte[rsi+rcx], r8b
		inc rcx
		cmp r8b, 0
		je .end_of_line
		jmp .copy_loop

	.buffer_error:
		mov rax, 0
		ret
	
	.end_of_line:
		mov rax, rcx
		ret




