%include "lib.inc"
%include "words.inc"
%define BUFFER 255

section .rodata

buf_error_message: db "Error: buffer overflow", 0
end_error_message: db "Error: key was not found", 0

section .bss
input_buffer: resb BUFFER

section .text

global _start
extern find_word

_start:

	mov rdi, rsp
	mov rsi, BUFFER
	call read_word
	test rax, rax
	jz .buf_error

	mov rdi, rsp
	mov rsi, NEXT_ELEM
	call find_word
	test rax, rax
	jz .end_error

	mov rdi, rax
	add rdi, 8
	call string_length
	inc rax
	add rdi, rax
	call print_string
	call exit

	.buf_error:
		mov rdi, buf_error_message
		call print_err_string
		call exit

	.end_error:
		mov rdi, end_error_message
		call print_err_string
		call exit

print_err_string:
	push rdi
	call string_length
	pop rdi
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, 2
	syscall
	ret
